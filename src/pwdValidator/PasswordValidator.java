/*
 * Trevor D'Souza || 991552924
 * This class takes in input from user and spits out if the password is valid or not and will be created using TDD
 */
package pwdValidator;

import java.util.Random;

public class PasswordValidator {

	private static int MIN_LENGTH = 8;

	public static boolean isValidLength(String password) {
		return password.indexOf(" ") < 0 && password.length() >= MIN_LENGTH;
	}

	public static boolean hasValidDigitCount(String password) {
		return password != null && password.matches("([^0-9]*[0-9]){2}.*");
	}
	
	public static boolean hasUpperCaseAndLowerCase(String password) {
		return password != null && password.matches("^(?=.*[a-z])(?=.*[A-Z]).+$");
	}

	protected static String generateRandomPassword() {
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		int size = (int) (rnd.nextFloat() * 16);
		while (salt.length() < size) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;

	}

	public static void main(String args[]) {
		System.out.println("Password Validator 1.0");
		String password = generateRandomPassword();
		System.out.println("Password generated for testing " + password);
		System.out.println("Is valid password ? : " + (isValidLength(password) && hasValidDigitCount(password) && hasUpperCaseAndLowerCase(password)));
	}

}