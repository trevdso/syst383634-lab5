/*
 * Trevor D'Souza || 991552924
 * This class takes in input from user and spits out if the password is valid or not and will be created using TDD
 */
package pwdValidator;

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {

	@Test
	public void testIsValidLengthRegular() {
		boolean result = PasswordValidator.isValidLength("123456789012");
		assertTrue("Invalid length", result);
	}

	@Test
	public void testIsValidLengthBoundaryIn() {
		boolean result = PasswordValidator.isValidLength("12345678");
		assertTrue("Invalid length", result);
	}

	@Test
	public void testIsValidLengthException() {
		boolean result = PasswordValidator.isValidLength("");
		assertFalse("Invalid length", result);
	}

	@Test
	public void testIsValidLengthExceptionSpaces() {
		boolean result = PasswordValidator.isValidLength("    t  e  s  t       ");
		assertFalse("Invalid length", result);
	}

	@Test
	public void testIsValidLengthBoundaryOut() {
		boolean result = PasswordValidator.isValidLength("1234567");
		assertFalse("Invalid length", result);
	}

	@Test
	public void testHasValidDigitCountRegular() {
		boolean result = PasswordValidator.hasValidDigitCount("ss4sd8s4ds");
		assertTrue("Invalid number of digits", result);
	}

	@Test
	public void testHasValidDigitCountBoundaryIn() {
		boolean result = PasswordValidator.hasValidDigitCount("kdk5jhjd4k8fjjjhjj");
		assertTrue("Invalid number of digits", result);
	}

	@Test
	public void testHasValidDigitCountException() {
		boolean result = PasswordValidator.hasValidDigitCount("kdkdkdkfj");
		assertFalse("Invalid number of digits", result);
	}

	@Test
	public void testHasValidDigitCountBoundaryOut() {
		boolean result = PasswordValidator.hasValidDigitCount("kdkd4kdkfj");
		assertFalse("Invalid number of digits", result);
	}
	
	@Test
	public void testHasUpperAndLowerRegular() {
		boolean result = PasswordValidator.hasUpperCaseAndLowerCase("sS4sd8S4ds");
		assertTrue("Requirements for Upper and Lower Case not met", result);
	}

	@Test
	public void testHasUpperAndLowerException() {
		boolean result = PasswordValidator.hasUpperCaseAndLowerCase("kdkdkdk332");
		assertFalse("Requirements for Upper and Lower Case not met", result);
	}
	
	@Test
	public void testHasUpperAndLowerBoundaryIn() {
		boolean result = PasswordValidator.hasUpperCaseAndLowerCase("kdK5jhjd4k8fjjjhjj");
		assertTrue("Requirements for Upper and Lower Case not met", result);
	}

	@Test
	public void testHasUpperAndLowerBoundaryOut() {
		boolean result = PasswordValidator.hasUpperCaseAndLowerCase("AJDH78293JASKa");
		assertTrue("Requirements for Upper and Lower Case not met", result);
	}

}
